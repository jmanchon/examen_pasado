import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'proyecto.settings')

import django
django.setup()
from aplicacion.models import paciente, medico, receta

def populate():
    m1 = add_medico('medico1')
    m2 = add_medico('medico2')
    m3 = add_medico('medico3')
    m4 = add_medico('medico4')
    
    p1 = add_paciente('paciente1')
    p2 = add_paciente('paciente2')

    add_receta(m1, p1)
    add_receta(m2, p1)
    add_receta(m1, p2)
    add_receta(m2, p2)
    add_receta(m3, p2)


def add_paciente(nombre):
    p = paciente.objects.get_or_create(nombreP=nombre)[0]
    p.save()
    return p

def add_medico(nombre):
    m = medico.objects.get_or_create(nombreM=nombre)[0]
    m.save()
    return m

def add_receta(medico, paciente):
    r = receta.objects.get_or_create(paciente=paciente, medico=medico)[0]
    r.save()
    return r

# Start execution here!
if __name__ == '__main__':
    print("Starting Rango population script...")
    populate()
