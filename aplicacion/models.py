from __future__ import unicode_literals

from django.db import models

# Create your models here.
class paciente(models.Model):
    nombreP = models.CharField(max_length=128)
    
class medico(models.Model):
    nombreM = models.CharField(max_length=128)
    
class receta(models.Model):
    paciente = models.ForeignKey(paciente)
    medico = models.ForeignKey(medico)    

